<?php
  // ROOT PAGE
  $GLOBALS['TL_LANG']['tl_page']['og_legend']         = 'Open Graph Tags';
  $GLOBALS['TL_LANG']['tl_page']['og_title']          = array('Titel','Der Titel des Objekts wie es auf Facebook erscheinen sollte.');
  $GLOBALS['TL_LANG']['tl_page']['og_type']           = array('Art','Die Art des Objekts z.B. "website".');
  $GLOBALS['TL_LANG']['tl_page']['og_image']          = array('Bild','Vorschaubild des Objekts.');
  $GLOBALS['TL_LANG']['tl_page']['og_description']    = array('Beschreibung','Eine kurze Beschreibung des Objekts.');

  $GLOBALS['TL_LANG']['tl_page']['tw_legend']         = 'Twitter Tags';
  $GLOBALS['TL_LANG']['tl_page']['tw_site']           = array('Seite', '@benutzername der Seite.');
  $GLOBALS['TL_LANG']['tl_page']['tw_creator']        = array('Autor', '@benutzername des Autors der Seite.');
  $GLOBALS['TL_LANG']['tl_page']['tw_card']           = array('Anzeigeart', 'Art der Karte, weitere Informationen unter https://dev.twitter.com/cards/overview.');
  $GLOBALS['TL_LANG']['tl_page']['tw_title']          = array('Titel', 'Ein passender Titel für den Inhalt der Seite.');
  $GLOBALS['TL_LANG']['tl_page']['tw_description']    = array('Beschreibung', 'Eine kurze Beschreibung des Objekts.');
  $GLOBALS['TL_LANG']['tl_page']['tw_image']          = array('Bild', 'Vorschaubild des Objekts.');
?>
