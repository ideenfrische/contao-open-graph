<?php
  $GLOBALS['TL_DCA']['tl_page']['palettes']['regular'] = str_replace(
    '{cache_legend:hide}',
    '{og_legend:hide},og_title,og_type,og_description,og_image;{tw_legend:hide},tw_site,tw_creator,tw_title,tw_card,tw_description,tw_image;{cache_legend:hide}',
    $GLOBALS['TL_DCA']['tl_page']['palettes']['regular']
  );
  $GLOBALS['TL_DCA']['tl_page']['palettes']['root'] = str_replace(
    '{cache_legend:hide}',
    '{og_legend:hide},og_type,og_image;{tw_legend:hide},tw_site,tw_creator,tw_card,tw_image;{cache_legend:hide}',
    $GLOBALS['TL_DCA']['tl_page']['palettes']['root']
  );
  $GLOBALS['TL_DCA']['tl_page']['palettes']['rootfallback'] = str_replace(
    '{cache_legend:hide}',
    '{og_legend:hide},og_type,og_image;{tw_legend:hide},tw_site,tw_creator,tw_card,tw_image;{cache_legend:hide}',
    $GLOBALS['TL_DCA']['tl_page']['palettes']['rootfallback']
  );


  // open graph
  $GLOBALS['TL_DCA']['tl_page']['fields']['og_title'] = array(
    'exclude'                 => true,
    'inputType'               => 'text',
    'eval'                    => array('tl_class'=>'w50'),
    'sql'                     => "varchar(255) NOT NULL default ''"
  );

  $GLOBALS['TL_DCA']['tl_page']['fields']['og_type'] = array(
    'exclude'                 => true,
    'inputType'               => 'select',
    'options'                 => array('website','article','book','business.business','music.album','music.song','place','product','profile'),
    'eval'                    => array('tl_class'=>'w50','includeBlankOption'=>true),
    'sql'                     => "varchar(255) NOT NULL default ''"
  );

  $GLOBALS['TL_DCA']['tl_page']['fields']['og_image'] = array(
    'exclude'                 => true,
    'inputType'               => 'fileTree',
    'eval'                    => array('fieldType'=>'radio', 'filesOnly'=>true, 'mandatory'=>false, 'tl_class'=>'clr'),
    'sql'                     => "binary(16) NULL"
  );

  $GLOBALS['TL_DCA']['tl_page']['fields']['og_description'] = array(
    'exclude'                 => true,
    'inputType'               => 'text',
    'eval'                    => array('tl_class'=>'long clr'),
    'sql'                     => "varchar(255) NOT NULL default ''"
  );

  // twitter
  $GLOBALS['TL_DCA']['tl_page']['fields']['tw_site'] = array(
    'exclude'                 => true,
    'inputType'               => 'text',
    'eval'                    => array('tl_class'=>'w50', 'placeholder'=>'@page'),
    'sql'                     => "varchar(255) NOT NULL default ''"
  );

  $GLOBALS['TL_DCA']['tl_page']['fields']['tw_creator'] = array(
    'exclude'                 => true,
    'inputType'               => 'text',
    'eval'                    => array('tl_class'=>'w50', 'placeholder'=>'@author'),
    'sql'                     => "varchar(255) NOT NULL default ''"
  );

  $GLOBALS['TL_DCA']['tl_page']['fields']['tw_card'] = array(
    'exclude'                 => true,
    'inputType'               => 'select',
    'options'                 => array('summary_large_image', 'summary'),
    'eval'                    => array('tl_class'=>'w50','includeBlankOption'=>true),
    'sql'                     => "varchar(255) NOT NULL default ''"
  );

  $GLOBALS['TL_DCA']['tl_page']['fields']['tw_title'] = array(
    'exclude'                 => true,
    'inputType'               => 'text',
    'eval'                    => array('tl_class'=>'w50','maxlength'=>70),
    'sql'                     => "varchar(255) NOT NULL default ''"
  );

  $GLOBALS['TL_DCA']['tl_page']['fields']['tw_description'] = array(
    'exclude'                 => true,
    'inputType'               => 'text',
    'eval'                    => array('tl_class'=>'long clr','maxlength'=>200),
    'sql'                     => "varchar(255) NOT NULL default ''"
  );

  $GLOBALS['TL_DCA']['tl_page']['fields']['tw_image'] = array(
    'exclude'                 => true,
    'inputType'               => 'fileTree',
    'eval'                    => array('fieldType'=>'radio', 'filesOnly'=>true, 'mandatory'=>false, 'tl_class'=>'clr'),
    'sql'                     => "binary(16) NULL"
  );
?>
