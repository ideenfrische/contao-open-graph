<?php
namespace ideenfrische\OgTagsBundle\EventListener;

use Contao\CoreBundle\ServiceAnnotation\Hook;
use Contao\PageRegular;
use Contao\LayoutModel;
use Contao\PageModel;
use Contao\FilesModel;
use Contao\Environment;


class OgTagsListener
{
  public function __invoke(PageModel $pageModel, LayoutModel $layout, PageRegular $pageRegular): void
  {
    $objRoot = PageModel::findByPk($pageModel->rootId);

    $html_og = "";
    $html_tw = "";

    // URL
    $html_og .= '<meta property="og:url" content="'.Environment::get('base').Environment::get('request').'" />';

    // Title
    $title = $pageModel->title;
    if($pageModel->pageTitle){
      $title = $pageModel->pageTitle;
    }
    $html_og .= '<meta property="og:title" content="'.($pageModel->og_title?:$title).'" />';
    $html_tw .= '<meta property="twitter:title" content="'.($pageModel->tw_title?:$title).'" />';
    // Description
    $description = $pageModel->description;
    $html_og .= '<meta property="og:description" content="'.($pageModel->og_description?:$description).'" />';
    $html_tw .= '<meta property="twitter:description" content="'.($pageModel->tw_description?:$description).'" />';

    // Other OG
    $og_type = $pageModel->og_type?:$objRoot->og_type;
    if($og_type){
      $html_og .= '<meta property="og:type" content="'.$og_type.'" />';
    }
    $og_image = $pageModel->og_image?:$objRoot->og_image;
    if($og_image){
      $objImage = FilesModel::findByPk($og_image);
      if($objImage){
        $html_og .= '<meta property="og:image" content="'.Environment::get('base').$objImage->path.'" />';
      }
    }
    // Other Twitter
    $tw_site = $pageModel->tw_site?:$objRoot->tw_site;
    $tw_creator = ($pageModel->tw_creator?:$objRoot->tw_creator)?:$tw_site;
    if($tw_site){
      $html_tw .= '<meta property="twitter:site" content="'.$tw_site.'" />';
    }
    if($tw_creator){
      $html_tw .= '<meta property="twitter:creator" content="'.$tw_creator.'" />';
    }
    $tw_card = $pageModel->tw_card?:$objRoot->tw_card;
    if($tw_card){
      $html_tw .= '<meta property="twitter:card" content="'.$tw_card.'" />';
    }
    $tw_image = $pageModel->tw_image?:$objRoot->tw_image;
    if($tw_image){
      $objImage = FilesModel::findByPk($tw_image);
      if($objImage){
        $html_tw .= '<meta property="twitter:image" content="'.Environment::get('base').$objImage->path.'" />';
      }
    }

    $GLOBALS['TL_HEAD']['og_tags'] = $html_og;
    $GLOBALS['TL_HEAD']['tw_tags'] = $html_tw;
  }
}
